# MAJIS operations toolbox history

All notable changes to MAJIS operations toolbox will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

[Unreleased][new]
-----------------
[new]: https://git.ias.u-psud.fr/majis_sgs/operations/majis-ops-toolbox/-/compare/1.0.0...main

### Added
- SonarQube code quality job in Gitlab CI ([!7](https://git.ias.u-psud.fr/majis_sgs/operations/majis-ops-toolbox/-/merge_requests/7))

### Changed
- Project status is now considered `stable`

### Fixed
- Fix invalid timeline template format ([!8](https://git.ias.u-psud.fr/majis_sgs/operations/majis-ops-toolbox/-/merge_requests/8)).
- Gitlab-CI unquoted hatch version.
- Gitlab-CI build project before PyPI publication.
- Gitlab-CI add user `__token__` value for PyPI publication.


[Release 1.0.0 - 2024/06/19][1.0.0]
-----------------------------------
[1.0.0]: https://git.ias.u-psud.fr/majis_sgs/operations/majis-ops-toolbox/-/releases/1.0.0

### Added

- Add `read_itl()`, `save_itl()`, `save_csv()` and `save_xlsm()` methods.
- Add `Timeline` object to manipulate MAJIS `.xlsm` timeline file.
- Add `majis-itl` command line interface.
- Project init
