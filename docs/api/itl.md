ITL submodule
=============

```{eval-rst}
.. automodule:: majis.itl.reader
   :members:
   :no-index: read_itl
.. automodule:: majis.itl.timeline
   :members:
   :no-index: Timeline
.. automodule:: majis.itl.export
   :members:
.. automodule:: majis.itl.cli
   :members:
```
