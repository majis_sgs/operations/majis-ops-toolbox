Miscellaneous API
=================

```{eval-rst}
.. automodule:: majis.misc.time
   :members:
.. automodule:: majis.misc.events
   :members:
.. automodule:: majis.misc.evf
   :members:
.. automodule:: majis.misc.csv
   :members:
```
