"""Test ITL CLI module."""

import sys

from majis.itl import cli

from pytest import fixture, raises


def stderr_dump(name: str, result: any) -> callable:
    """Dump output to stdout"""

    def func(*args, **kwargs):
        s = []
        for arg in args:
            s.append(str(arg))
        for key, value in kwargs.items():
            s.append(f'{key}={value}')

        sys.stderr.write(f'{name}({", ".join(s)}) -> {result}\n')
        return result

    return func


@fixture
def mock_func(monkeypatch):
    """Mock module functions."""
    monkeypatch.setattr(cli, 'read_evf', stderr_dump('read_evf', 'refs'))
    monkeypatch.setattr(cli, 'read_itl', stderr_dump('read_itl', 'events'))
    monkeypatch.setattr(cli, 'save_itl', stderr_dump('save_itl', ['# DUMMY ITL']))
    monkeypatch.setattr(cli, 'save_csv', stderr_dump('save_csv', ['# DUMMY CSV']))
    monkeypatch.setattr(cli, 'save_xlsm', stderr_dump('save_xlsm', ['# DUMMY XLSM']))


def test_itl_cli_single_file_display_itl(capsys, mock_func):
    """Test ITL CLI with single file export to ITL."""
    cli.cli('foo.itl'.split())

    out, err = capsys.readouterr()

    assert out == '# DUMMY ITL\n'
    assert err == '\n'.join(
        [
            'read_evf(None) -> refs',
            'read_itl(foo.itl, refs=refs, flat=True) -> events',
            "save_itl(None, events, ref=None, header=None, overlap=False) -> ['# DUMMY ITL']",
            '',
        ]
    )


def test_itl_cli_multiple_files_display_csv(capsys, mock_func):
    """Test ITL CLI with multiple files export to ITL."""
    cli.cli('abs.itl rel.itl --time-ref events.evf --csv'.split())

    out, err = capsys.readouterr()

    assert out == '# DUMMY CSV\n'
    assert err == '\n'.join(
        [
            'read_evf(events.evf) -> refs',
            'read_itl(abs.itl, refs=refs, flat=True) -> events',
            'read_itl(rel.itl, refs=refs, flat=True) -> events',
            "save_csv(None, events, events, sep=;, overlap=False) -> ['# DUMMY CSV']",
            '',
        ]
    )


def test_itl_cli_save_relative_itl(capsys, mock_func):
    """Test ITL CLI export to relative ITL."""
    cli.cli(
        [
            'abs.itl',
            '--output',
            'output.itl',
            '--relative-to',
            '"22-JAN-2031_18:54:03 CA_EUROPA (COUNT = 1)"',
            '--header',
            '"# DUMMY HEADER"',
        ]
    )

    out, err = capsys.readouterr()

    assert out == 'ITL saved in: output.itl\n'
    assert err == '\n'.join(
        [
            'read_evf(None) -> refs',
            'read_itl(abs.itl, refs=refs, flat=True) -> events',
            'save_itl(output.itl, events, ref="22-JAN-2031_18:54:03 CA_EUROPA (COUNT = 1)", header="# DUMMY HEADER", overlap=False) -> [\'# DUMMY ITL\']',
            '',
        ]
    )


def test_itl_cli_save_abs_csv(capsys, mock_func):
    """Test ITL CLI export to CSV with only absolute time."""
    cli.cli(
        [
            'rel.itl',
            '--output',
            'output.csv',
            '--time-ref',
            '"2032-09-24T21:33:36 PERIJOVE_12PJ (COUNT = 1)"',
            '--csv-sep',
            '"|"',
            '--overlap',
        ]
    )

    out, err = capsys.readouterr()

    assert out == 'ITL saved in: output.csv\n'
    assert err == '\n'.join(
        [
            'read_evf("2032-09-24T21:33:36 PERIJOVE_12PJ (COUNT = 1)") -> refs',
            'read_itl(rel.itl, refs=refs, flat=True) -> events',
            'save_csv(output.csv, events, ref=None, sep="|", overlap=True) -> [\'# DUMMY CSV\']',
            '',
        ]
    )


def test_itl_cli_save_rel_csv(capsys, mock_func):
    """Test ITL CLI export to CSV with absolute and relative times."""
    cli.cli(
        [
            'rel.itl',
            '--output',
            'output.csv',
            '--relative-to',
            '"01-JAN-2032_00:00:02 EVENT_NAME (COUNT = 1)"',
        ]
    )

    out, err = capsys.readouterr()

    assert out == 'ITL saved in: output.csv\n'
    assert err == '\n'.join(
        [
            'read_evf(None) -> refs',
            'read_itl(rel.itl, refs=refs, flat=True) -> events',
            'save_csv(output.csv, events, ref="01-JAN-2032_00:00:02 EVENT_NAME (COUNT = 1)", sep=;, overlap=False) -> [\'# DUMMY CSV\']',
            '',
        ]
    )


def test_itl_cli_save_xlsm_from_default_template(capsys, mock_func):
    """Test ITL CLI export XLSM timeline from default template."""
    cli.cli(
        [
            'abs.itl',
            '--output',
            'output.xlsm',
        ]
    )

    out, err = capsys.readouterr()

    assert out == 'ITL saved in: output.xlsm\n'
    assert err == '\n'.join(
        [
            'read_evf(None) -> refs',
            'read_itl(abs.itl, refs=refs, flat=True) -> events',
            "save_xlsm(output.xlsm, events, timeline=None, ca_ref=None, overlap=False) -> ['# DUMMY XLSM']",
            '',
        ]
    )


def test_itl_cli_save_xlsm_from_existing_template(capsys, mock_func):
    """Test ITL CLI export XLSM timeline from existing template."""
    cli.cli(
        [
            '--timeline',
            'timeline.xlsm',
            '--relative-to',
            '"22-JAN-2031_18:54:03 CA_EUROPA (COUNT = 1)"',
        ]
    )

    out, err = capsys.readouterr()

    assert out == 'ITL appended to: timeline.xlsm\n'
    assert err == '\n'.join(
        [
            'read_evf(None) -> refs',
            'save_xlsm(None, timeline=timeline.xlsm, ca_ref="22-JAN-2031_18:54:03 CA_EUROPA (COUNT = 1)", overlap=False) -> [\'# DUMMY XLSM\']',
            '',
        ]
    )


def test_itl_cli_errors(mock_func):
    """Test ITL CLI export to absolute CSV."""
    # Avoid overwrite existing files
    with raises(FileExistsError):
        cli.cli(f'abs.itl --output {__file__}'.split())

    # Invalid output extension
    err = r'Only `.itl`|`.csv`|`.xlsm` extension are accepted \(not `.xyz`\)'
    with raises(ValueError, match=err):
        cli.cli('abs.itl --output foo.xyz'.split())
