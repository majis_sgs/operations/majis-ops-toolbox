"""Test ITL export module."""

from planetary_coverage.events import EventsDict, EventsList, EventWindow

from majis.itl import save_csv, save_itl, save_xlsm
from majis.itl.export import Path, Timeline

from pytest import raises


def event(i, **kwargs):
    """Dummy event object."""
    event = EventWindow(
        'DISK_SCAN',
        t_start=f'2032-01-01T00:00:{i:02d}',
        t_end=f'2032-01-01T00:00:{i + 1:02d}',
        INSTRUMENT='MAJIS',
        SCENARIO='S007_01',
        OBS_NAME=f'DISK_SCAN_{i:03d}',
        TARGET='JUPITER',
        PRIME=True,
        **kwargs,
    )
    event.comments = [
        f'# MAJIS - SCENARIO=S007_01 OBS_NAME=DISK_SCAN_{i:03d}',
        '# MAJIS - TARGET=JUPITER',
    ]
    return event


def test_itl_save_absolute_itl():
    """Test ITL export as absolute time."""
    assert save_itl(None, EventsList([event(1), event(2)]), header='# FOO BAR BAZ') == [
        '# FOO BAR BAZ',
        '# MAJIS - SCENARIO=S007_01 OBS_NAME=DISK_SCAN_001',
        '# MAJIS - TARGET=JUPITER',
        '2032-01-01T00:00:01.000Z  MAJIS  OBS_START  DISK_SCAN (PRIME=TRUE)',
        '2032-01-01T00:00:02.000Z  MAJIS  OBS_END    DISK_SCAN',
        '',
        '# MAJIS - SCENARIO=S007_01 OBS_NAME=DISK_SCAN_002',
        '# MAJIS - TARGET=JUPITER',
        '2032-01-01T00:00:02.000Z  MAJIS  OBS_START  DISK_SCAN (PRIME=TRUE)',
        '2032-01-01T00:00:03.000Z  MAJIS  OBS_END    DISK_SCAN',
        '',
    ]


def test_itl_save_relative_itl():
    """Test ITL export as relative time."""
    assert save_itl(
        None,
        EventsDict([event(1), event(2)]),
        ref='01-JAN-2032_00:00:02 EVENT_NAME (COUNT = 1)',
    ) == [
        '# Relative time reference:',
        '# 2032-01-01T00:00:02  EVENT_NAME (COUNT = 1)',
        '',
        '# MAJIS - SCENARIO=S007_01 OBS_NAME=DISK_SCAN_001',
        '# MAJIS - TARGET=JUPITER',
        'EVENT_NAME (COUNT = 1)  -000.00:00:01.000  MAJIS  OBS_START  DISK_SCAN (PRIME=TRUE)',
        'EVENT_NAME (COUNT = 1)  +000.00:00:00.000  MAJIS  OBS_END    DISK_SCAN',
        '',
        '# MAJIS - SCENARIO=S007_01 OBS_NAME=DISK_SCAN_002',
        '# MAJIS - TARGET=JUPITER',
        'EVENT_NAME (COUNT = 1)  +000.00:00:00.000  MAJIS  OBS_START  DISK_SCAN (PRIME=TRUE)',
        'EVENT_NAME (COUNT = 1)  +000.00:00:01.000  MAJIS  OBS_END    DISK_SCAN',
        '',
    ]


def test_itl_save_csv():
    """Test ITL export as csv."""
    assert save_csv(None, EventsList([event(1), event(2)])) == [
        '#OBS_NAME;OBS_START;OBS_END;INSTRUMENT;SCENARIO;TARGET;PRIME',
        'DISK_SCAN_001;2032-01-01T00:00:01.000Z;2032-01-01T00:00:02.000Z;MAJIS;S007_01;JUPITER;True',
        'DISK_SCAN_002;2032-01-01T00:00:02.000Z;2032-01-01T00:00:03.000Z;MAJIS;S007_01;JUPITER;True',
    ]


def test_itl_save_csv_with_relative_time():
    """Test ITL export as csv with relative time."""
    assert save_csv(
        None,
        EventsList([event(1), event(2)]),
        ref='01-JAN-2032_00:00:02 EVENT_NAME (COUNT = 1)',
    ) == [
        '#OBS_NAME;OBS_START;OBS_END;OBS_START_REL;OBS_END_REL;INSTRUMENT;SCENARIO;TARGET;PRIME',
        'DISK_SCAN_001;2032-01-01T00:00:01.000Z;2032-01-01T00:00:02.000Z;EVENT_NAME (COUNT = 1)  -000.00:00:01.000;EVENT_NAME (COUNT = 1)  +000.00:00:00.000;MAJIS;S007_01;JUPITER;True',
        'DISK_SCAN_002;2032-01-01T00:00:02.000Z;2032-01-01T00:00:03.000Z;EVENT_NAME (COUNT = 1)  +000.00:00:00.000;EVENT_NAME (COUNT = 1)  +000.00:00:01.000;MAJIS;S007_01;JUPITER;True',
    ]


def test_itl_save_xlsm(monkeypatch):
    """Test ITL export as XLSM timeline."""
    # Mock Timeline.save() method to return directly the timeline itself
    monkeypatch.setattr(Timeline, 'save', lambda self, _: self)

    # Missing required parameters
    attrs = {
        'START_ANGLE': '-2.0',
        'START_SCAN_SPEED': '-0.1',
        'STOP_SCAN_SPEED': '+0.1',
        'SYNCHRONOUS': '+3',
        'STOP_ANGLE': '+20',
        'CU_TREP': '500ms',
        'BINNING': '2',
        'CU_FRAME': '256',
        'PPE': '64',
        'START_ROW_VIS': '100',
        'COMMENTS': None,
        'ITL': None,
    }

    out = save_xlsm(None, EventsList([event(1, **attrs), event(2, **attrs)]))

    assert len(out) == 2

    assert out['OBS_NAME'] == [
        'DISK_SCAN_001',
        'DISK_SCAN_002',
    ]

    assert out['First CU_frame start (UTC)'] == [
        '2032-01-01T00:00:01.000Z',
        '2032-01-01T00:00:02.000Z',
    ]


def test_itl_save_errors(monkeypatch):
    """Test export errors."""
    monkeypatch.setattr(Path, 'write_text', lambda *_: None)

    assert str(save_itl('foo.itl', EventsList([event(1), event(2)]))) == 'foo.itl'

    # Invalid file extension
    err = r'Output file name should ends with `\.itl`: `bar\.txt` provided\.'
    with raises(ValueError, match=err):
        save_itl('bar.txt', EventsList([event(1), event(2)]))

    # Block overlap
    err = r'Overlap between `DISK_SCAN \(2032-01-01 -> 2032-01-01\)` and `DISK_SCAN \(2032-01-01 -> 2032-01-01\)`'
    with raises(ValueError, match=err):
        save_itl('baz.itl', EventsList([event(1), event(1)]))
