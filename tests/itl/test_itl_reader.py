"""Test ILT reader module."""

from pathlib import Path

from numpy import datetime64 as dt

from planetary_coverage.events import EventsDict, EventsList, EventWindow

from majis import read_itl
from majis.itl.reader import _parse_itl

from pytest import raises

DATA = Path(__file__).parent / '..' / 'data'


def test_itl_reader_abs_events_dict():
    """Test ITL reader absolute events dict."""
    events = read_itl(DATA / 'absolute_time.itl')

    assert isinstance(events, EventsDict)
    assert len(events) == 1

    assert 'MAJ_JUP_DISK_SCAN' in events

    jup_disk_scan_blocks = events['MAJ_JUP_DISK_SCAN']

    assert isinstance(jup_disk_scan_blocks, EventsList)
    assert len(jup_disk_scan_blocks) == 2

    obs = jup_disk_scan_blocks[0]

    assert isinstance(obs, EventWindow)
    assert obs.key == 'MAJ_JUP_DISK_SCAN'
    assert obs.start == dt('2032-09-23T05:15:45')
    assert obs.stop == dt('2032-09-23T05:26:15')

    assert obs['INSTRUMENT'] == 'MAJIS'
    assert obs['PRIME']

    assert obs['SCENARIO'] == 'S007_01'
    assert obs['OBS_NAME'] == 'MAJ_JUP_DISK_SCAN_001'
    assert obs['TARGET'] == 'JUPITER'
    assert obs['CU_TREP'] == '500ms'
    assert obs['CU_FRAME'] == '300'
    assert obs['BINNING'] == '1'
    assert obs['PPE'] == '400'
    assert obs['START_ANGLE'] == '-1.31051'
    assert obs['STOP_ANGLE'] == '+0.10202'
    assert obs['SYNCHRONOUS'] == '0'
    assert obs['START_SCAN_SPEED'] == '+0.0022421078'
    assert obs['STOP_SCAN_SPEED'] == '+0.0022421078'

    assert obs['COMMENTS'] == 'MULTI WORDS COMMENT with , and ; / MULTI LINES COMMENT'

    assert obs.comments == [
        '# MAJIS - SCENARIO=S007_01 OBS_NAME=MAJ_JUP_DISK_SCAN_001 TARGET=JUPITER CU_TREP=500ms CU_FRAME=300 BINNING=1 PPE=400 START_ROW_VIS=100',
        '# MAJIS - START_ANGLE=-1.31051 STOP_ANGLE=+0.10202 SYNCHRONOUS=0 START_SCAN_SPEED=+0.0022421078 STOP_SCAN_SPEED=+0.0022421078',
        '# MAJIS - COMMENTS : MULTI WORDS COMMENT with , and ;',
        '# MAJIS - COMMENTS : MULTI LINES COMMENT',
    ]

    assert obs['ITL'].name == 'absolute_time.itl'


def test_itl_reader_abs_events_list():
    """Test ITL reader absolute events list."""
    events = read_itl(DATA / 'absolute_time.itl', flat=True)

    assert isinstance(events, EventsList)
    assert len(events) == 2

    assert [event['OBS_NAME'] for event in events] == [
        'MAJ_JUP_DISK_SCAN_001',
        'MAJ_JUP_DISK_SCAN_002',
    ]


def test_itl_reader_rel_events_dict():
    """Test ITL reader relative events dict."""
    itl = read_itl(DATA / 'relative_time.itl', refs=DATA / 'events.evf')

    assert isinstance(itl, EventsDict)

    obs = itl['MAJ_JUP_DISK_SCAN'][0]

    assert isinstance(obs, EventWindow)
    assert obs.start == dt('2032-09-23T02:58:11')
    assert obs.stop == dt('2032-09-23T03:01:59.900')


def test_itl_reader_rel_events_list():
    """Test ITL reader relative events list."""
    events = read_itl(
        DATA / 'relative_time.itl',
        refs='24-SEP-2032_21:33:36 PERIJOVE_12PJ (COUNT = 1)',
        flat=True,
    )

    assert isinstance(events, EventsList)

    assert [event.start for event in events] == [
        dt('2032-09-23T02:58:11'),
        dt('2032-09-23T03:04:34'),
    ]

    assert [event.stop for event in events] == [
        dt('2032-09-23T03:01:59.900'),
        dt('2032-09-23T03:08:22.900'),
    ]


def test_itl_reader_errors():
    """Test ITL reader errors."""
    # File not found
    with raises(FileNotFoundError):
        _ = read_itl('foo.itl')

    # Relative time without references
    err = r'Unknown time reference: `PERIJOVE_12PJ \(COUNT = 1\)`'
    with raises(KeyError, match=err):
        _ = read_itl(DATA / 'relative_time.itl')

    # Missing observation instrument
    err = r'Missing instrument in: `2032-01-01T00:00:00  OBS_START `'
    with raises(ValueError, match=err):
        _parse_itl(['2032-01-01T00:00:00  OBS_START '])

    # Missing observation key
    err = r'Missing obs name in: `2032-01-01T00:00:00  MAJIS   OBS_END `'
    with raises(ValueError, match=err):
        _parse_itl(['2032-01-01T00:00:00  MAJIS   OBS_END '])

    # Mismatch instrument names in block
    err = r'Instrument block mismatch: `MAJIS` / `JANUS`'
    with raises(ValueError, match=err):
        _parse_itl(
            [
                '2032-01-01T00:00:00  MAJIS   OBS_START  DISK_SCAN',
                '2032-01-02T00:00:00  JANUS   OBS_END    DISK_SCAN',
            ]
        )

    # Mismatch obs names in block
    err = r'Obs name block mismatch: `DISK_SCAN` / `LIMB_SCAN`'
    with raises(ValueError, match=err):
        _parse_itl(
            [
                '2032-01-01T00:00:00  MAJIS   OBS_START  DISK_SCAN',
                '2032-01-02T00:00:00  MAJIS   OBS_END    LIMB_SCAN',
            ]
        )
