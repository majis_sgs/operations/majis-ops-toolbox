"""Test MAJIS ITL timeline sub-module."""

from pathlib import Path

from packaging.version import Version

from planetary_coverage.html import display_html

from majis import Timeline, read_itl
from majis.itl.timeline import DEFAULT_TEMPLATE, TimelineChangeLog

from pytest import raises

DATA = (Path(__file__).parent / '..' / 'data').resolve()

TEMPLATE_LATEST_VERSION = Version('2.0')
CA_REF = '24-SEP-2032_21:33:36 PERIJOVE_12PJ (COUNT = 1)'


def test_itl_timeline_default_template():
    """Test timeline default template."""
    timeline = Timeline()

    assert timeline.fname == DEFAULT_TEMPLATE
    assert not len(timeline)

    assert 'OBS_NAME' in timeline.fields
    assert timeline.fields['OBS_NAME'] == 'E'

    # Template structure
    assert timeline.version == TEMPLATE_LATEST_VERSION
    assert timeline.header == 3


def test_itl_timeline_from_absolute_itl():
    """Test timeline template from absolute ITL file."""
    timeline = Timeline(DATA / 'absolute_time.itl')

    assert timeline.fname == DEFAULT_TEMPLATE

    assert len(timeline) == 2

    # Item getter with string (column field formatted values)
    assert timeline['OBS_NAME'] == [
        'MAJ_JUP_DISK_SCAN_001',
        'MAJ_JUP_DISK_SCAN_002',
    ]

    assert timeline['start_angle'] == [
        -1.31051,
        +1.32935,
    ]

    assert timeline['First CU_frame start (UTC)'] == [
        '2032-09-23T05:15:45.000Z',
        '2032-09-23T06:09:45.000Z',
    ]

    assert timeline['cu_trep_ms'] == [
        500,
        2100,
    ]

    assert timeline['spatial_binning'] == [
        'No Binning',
        'Binning x2',
    ]

    assert timeline['Start Row VI'] == [
        100,
        372,
    ]

    assert timeline['prime'] == [
        'MAJIS',
        'MAJIS',
    ]

    assert timeline['Comments'] == [
        'MULTI WORDS COMMENT with , and ; / MULTI LINES COMMENT',
        None,  # Empty value should not be trimmed
    ]

    # Item getter with integer (observation index)
    obs = timeline[1]

    assert isinstance(obs, dict)
    assert obs['OBS_NAME'] == 'MAJ_JUP_DISK_SCAN_001'
    assert obs['First CU_frame start (UTC)'] == '2032-09-23T05:15:45.000Z'

    # Item getter with string and integer (observation formatter value)
    assert timeline['OBS_NAME', 1] == 'MAJ_JUP_DISK_SCAN_001'
    assert timeline['First CU_frame start (UTC)', 2] == '2032-09-23T06:09:45.000Z'

    # Item getter errors
    with raises(KeyError, match='Unknown `FOO` key'):
        timeline['FOO']

    with raises(IndexError, match='Invalid index: 3 not between 1 and 2'):
        timeline[3]

    with raises(
        TypeError,
        match=r'Only `str`, `int` or `str, int` are accepted \(`float` provided\)',
    ):
        timeline[1.0]

    # Item setter
    timeline['OBS_NAME', 1] = 'FOO'
    assert timeline['OBS_NAME', 1] == 'FOO'


def test_itl_timeline_from_relative_itl():
    """Test timeline template from relative ITL file."""
    timeline = Timeline(
        DATA / 'relative_time.itl',
        refs=DATA / 'events.evf',
        ca_ref=CA_REF,
    )

    assert len(timeline) == 2

    assert timeline['OBS_NAME'] == [
        'MAJ_JUP_DISK_SCAN_101',
        'MAJ_JUP_DISK_SCAN_102',
    ]

    assert timeline['First CU_frame start (UTC)'] == [
        '2032-09-23T02:58:11.000Z',
        '2032-09-23T03:04:34.000Z',
    ]

    assert timeline['First CU_frame start wrt C/A'] == [
        '-001.18:35:25.000',
        '-001.18:29:02.000',
    ]

    assert timeline['Last CU_frame stop wrt C/A'] == [
        '-001.18:31:36.100',
        '-001.18:25:13.100',
    ]


def test_itl_timeline_from_events():
    """Test timeline template from events objects."""
    events = read_itl(
        DATA / 'absolute_time.itl',
        refs=DATA / 'events.evf',
    )

    timeline = Timeline(events)

    assert len(timeline) == 2

    assert timeline['OBS_NAME'] == [
        'MAJ_JUP_DISK_SCAN_001',
        'MAJ_JUP_DISK_SCAN_002',
    ]

    assert timeline['ITL name'] == [
        'absolute_time.itl',
        'absolute_time.itl',
    ]


def test_itl_timeline_save():
    """Test timeline template save."""
    fout = DATA / 'output_timeline.xlsm'
    fout.unlink(missing_ok=True)
    assert not fout.exists()

    # Use default template
    timeline = Timeline(
        DATA / 'relative_time.itl',
        refs=DATA / 'events.evf',
        ca_ref=CA_REF,
    )

    assert timeline.save(fout) == fout
    assert fout.exists()

    # Export default template requires an explicit filename
    with raises(FileExistsError):
        timeline.save()

    # Load data from exported timeline
    timeline = Timeline(timeline=fout)

    assert timeline['OBS_NAME'] == [
        'MAJ_JUP_DISK_SCAN_101',
        'MAJ_JUP_DISK_SCAN_102',
    ]

    # Append new data to the existing timeline
    timeline.append(DATA / 'absolute_time.itl')

    # Export in loaded timeline without an explicit filename
    assert timeline.save() == fout

    # Reload data from exported timeline with both ITL (abs + rel)
    timeline = Timeline(timeline=fout)

    assert timeline['OBS_NAME'] == [
        'MAJ_JUP_DISK_SCAN_101',
        'MAJ_JUP_DISK_SCAN_102',
        'MAJ_JUP_DISK_SCAN_001',
        'MAJ_JUP_DISK_SCAN_002',
    ]

    assert timeline['First CU_frame start wrt C/A'] == [
        '-001.18:35:25.000',
        '-001.18:29:02.000',
        None,
        None,
    ]

    assert timeline['ITL name'] == [
        'relative_time.itl',
        'relative_time.itl',
        'absolute_time.itl',
        'absolute_time.itl',
    ]

    # Recompute relative time w.r.t. C/A with reference provided
    timeline = Timeline(timeline=fout, ca_ref=CA_REF)

    assert timeline['First CU_frame start wrt C/A'] == [
        '-001.18:35:25.000',
        '-001.18:29:02.000',
        '-001.16:17:51.000',
        '-001.15:23:51.000',
    ]

    # Remove output timeline
    fout.unlink()


def test_itl_timeline_template_changelog():
    """Test timeline template changelog."""
    log = Timeline().log

    assert isinstance(log, TimelineChangeLog)

    assert str(log).splitlines()[-3:] == [
        '',
        '>>>    1.0 | 2022-11-08 | François Poulet',
        'creation',
    ]

    first_log = log[-1]

    assert first_log.version == Version('1.0')
    assert first_log.date == '2022-11-08'
    assert first_log.author == 'François Poulet'
    assert first_log.change == 'creation'

    # __iter__ (discard lines with `…` and all fields are not empty strings)
    for version, date, author, change in log:
        assert date
        assert isinstance(date, str)
        assert version
        assert isinstance(version, Version)
        assert author
        assert isinstance(author, str)
        assert change != '…'
        assert isinstance(change, str)


def test_itl_timeline_science_changelog():
    """Test timeline science changelog."""
    sc = Timeline().science

    assert isinstance(sc, TimelineChangeLog)

    assert not str(sc)  # Empty by default


def test_itl_timeline_old_template():
    """Test old timeline schema for retro-compatibility."""
    # Prior to 2.0:
    # - Timeline header has a size of 2
    # - Science changelog is not available

    timeline = Timeline(timeline=DATA / 'old_templates' / 'timeline_1_9_10.xlsm')

    assert timeline.version == Version('1.9.10')
    assert timeline.header == 2

    with raises(ValueError):
        _ = timeline.science


def test_itl_timeline_html_representation():
    """Test timeline HTML representation."""
    timeline = Timeline(DATA / 'absolute_time.itl')

    assert display_html(timeline).startswith(
        '<table>'
        '<thead>'
        '<tr>'
        '<th>Event Name</th>'
        '<th>Phase</th>'
        '<th>block</th>'
        '<th>Comments</th>'
        '<th>OBS_NAME</th>'
    )

    assert (
        (
            '</th></tr></thead><tbody>'  # End of header
            '<tr>'  # New line
            '<td/>'  # Event Name
            '<td/>'  # Phase
            '<td/>'  # block
            '<td>MULTI WORDS COMMENT with , and ; / MULTI LINES COMMENT</td>'  # Comments
            '<td>MAJ_JUP_DISK_SCAN_001</td>'  # OBS_NAME
        )
        in display_html(timeline)
    )

    assert (
        (
            '</tr>'  # End of previous line
            '<tr>'  # New line
            '<td/>'  # Event Name
            '<td/>'  # Phase
            '<td/>'  # block
            '<td/>'  # Comments
            '<td>MAJ_JUP_DISK_SCAN_002</td>'  # OBS_NAME
        )
        in display_html(timeline)
    )

    assert display_html(timeline).endswith('</tr></tbody></table>')

    assert display_html(timeline.log).startswith(
        '<table>'
        '<thead>'
        '<tr>'
        '<th>Version</th>'
        '<th>Date</th>'
        '<th>Author</th>'
        '<th>Changes</th>'
        '</tr>'
        '</thead>'
        '<tbody>'
    )

    assert display_html(timeline.log).endswith(
        '<tr>'
        '<td>1.0</td>'
        '<td>2022-11-08</td>'
        '<td>François Poulet</td>'
        '<td>creation</td>'
        '</tr>'
        '</tbody>'
        '</table>'
    )
