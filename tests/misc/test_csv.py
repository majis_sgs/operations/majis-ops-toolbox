"""Test CSV miscellaneous module."""

from planetary_coverage.events import EventsDict, EventsList, EventWindow

from majis.misc import fmt_csv
from majis.misc.csv import get_header, get_value

from pytest import raises


def test_csv_get_header():
    """Test CSV header getter."""
    assert get_header([['t_start', 't_end']]) == ['OBS_START', 'OBS_END']

    assert get_header(
        EventsList(
            [
                EventWindow(
                    'event-a',
                    t_start=None,
                    t_end=None,
                    FOO=None,
                    COMMENTS=None,
                    OBS_NAME=None,
                ),
                EventWindow(
                    'event-b',
                    t_start=None,
                    t_end=None,
                    BAR=None,
                ),
            ]
        ),
        ref=True,
    ) == [
        'OBS_NAME',
        'OBS_START',
        'OBS_END',
        'OBS_START_REL',
        'OBS_END_REL',
        'FOO',
        'BAR',
        'COMMENTS',
    ]

    # Errors
    with raises(KeyError, match='Missing `t_start` keyword.'):
        get_header([[]])

    with raises(KeyError, match='Missing `t_end` keyword.'):
        get_header([['t_start']])


def test_csv_get_value():
    """Test CSV value getter."""
    event = EventWindow(
        'event-a',
        t_start='2020-JAN-01 12:34:56',
        t_end='2020-01-02T12:34:56.789',
        OBS_NAME='FOO',
        BAR='BAZ',
        COMMENTS='QUX;QUUX,CORGE',
    )

    assert get_value(event, key='OBS_START') == '2020-01-01T12:34:56.000Z'
    assert get_value(event, key='OBS_END') == '2020-01-02T12:34:56.789Z'
    assert get_value(event, key='OBS_NAME') == 'FOO'
    assert get_value(event, key='COMMENTS') == '"QUX;QUUX,CORGE"'
    assert get_value(event, key='QUUX') == ''

    assert (
        get_value(
            event, key='OBS_START_REL', ref='01-JAN-2020_00:00:00 EVENT (COUNT = 1)'
        )
        == 'EVENT (COUNT = 1)  +000.12:34:56.000'
    )

    assert (
        get_value(
            event, key='OBS_END_REL', ref='01-JAN-2020_00:00:00.000 EVENT (COUNT = 1)'
        )
        == 'EVENT (COUNT = 1)  +001.12:34:56.789'
    )


def test_csv_formatter():
    """Test CSV formatter."""
    events = EventsDict(
        [
            EventWindow(
                'event-a',
                t_start='2020-01-01T12:34:56',
                t_end='2020-01-02T12:34:56.789',
                OBS_NAME='FOO',
                COMMENTS='QUX;QUUX,CORGE',
            ),
            EventWindow(
                'event-b',
                t_start='2020-02-01T12:34:56',
                t_end='2020-02-02T12:34:56.789',
                BAR='BAZ',
            ),
        ]
    )

    assert fmt_csv(events, sep=',') == [
        '#OBS_NAME,OBS_START,OBS_END,BAR,COMMENTS',
        'FOO,2020-01-01T12:34:56.000Z,2020-01-02T12:34:56.789Z,,"QUX;QUUX,CORGE"',
        ',2020-02-01T12:34:56.000Z,2020-02-02T12:34:56.789Z,BAZ,""',
    ]

    assert fmt_csv(events, ref='01-JAN-2020_00:00:00.000 EVENT (COUNT = 1)') == [
        '#OBS_NAME;OBS_START;OBS_END;OBS_START_REL;OBS_END_REL;BAR;COMMENTS',
        'FOO;2020-01-01T12:34:56.000Z;2020-01-02T12:34:56.789Z;EVENT (COUNT = 1)  +000.12:34:56.000;EVENT (COUNT = 1)  +001.12:34:56.789;;"QUX;QUUX,CORGE"',
        ';2020-02-01T12:34:56.000Z;2020-02-02T12:34:56.789Z;EVENT (COUNT = 1)  +031.12:34:56.000;EVENT (COUNT = 1)  +032.12:34:56.789;BAZ;""',
    ]
