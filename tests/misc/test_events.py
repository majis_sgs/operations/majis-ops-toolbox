"""Test events module."""

from numpy import datetime64 as dt

from planetary_coverage.events.event import (
    EventsDict,
    EventsList,
    EventWindow,
)

from majis.misc.events import concatenate, flatten, group

from pytest import raises


def test_events_flatten():
    """Test events flatten method."""
    events_list = [
        EventWindow('foo', t_start='2032-01-01', t_end='2032-01-02'),
        EventWindow('foo', t_start='2031-01-01', t_end='2031-01-02'),
        EventWindow('bar', t_start='2030-01-01', t_end='2030-01-02'),
        EventWindow('baz', t_start='2033-01-01', t_end='2033-01-02'),
    ]

    # Multi events
    funcs = [
        list,
        tuple,
        EventsList,
        EventsDict,
    ]

    for func in funcs:
        events = flatten(func(events_list))

        assert isinstance(events, EventsList)

        assert [event.key for event in events] == ['bar', 'foo', 'foo', 'baz']
        assert [event.start for event in events] == [
            dt('2030-01-01'),
            dt('2031-01-01'),
            dt('2032-01-01'),
            dt('2033-01-01'),
        ]

    # Multi events in a tuple
    events = flatten(
        (
            EventsDict(
                [
                    EventWindow('foo', t_start='2032-01-01', t_end='2032-01-02'),
                    EventWindow('foo', t_start='2031-01-01', t_end='2031-01-02'),
                ]
            ),
            EventsList(
                [
                    EventWindow('bar', t_start='2033-01-01', t_end='2033-01-02'),
                    EventWindow('baz', t_start='2030-01-01', t_end='2030-01-02'),
                ]
            ),
        )
    )

    assert len(events) == 4

    assert [event.key for event in events] == ['baz', 'foo', 'foo', 'bar']
    assert [event.start for event in events] == [
        dt('2030-01-01'),
        dt('2031-01-01'),
        dt('2032-01-01'),
        dt('2033-01-01'),
    ]

    # Single event
    events = flatten(EventWindow('foo', t_start='2032-01-01', t_end='2032-01-02'))

    assert isinstance(events, EventsList)
    assert events[0].key == 'foo'

    # Invalid type
    with raises(TypeError):
        flatten('foo')


def test_events_group():
    """Test events group method."""
    events_list = [
        EventWindow('foo', t_start='2032-01-01', t_end='2032-01-02'),
        EventWindow('foo', t_start='2031-01-01', t_end='2031-01-02'),
        EventWindow('bar', t_start='2030-01-01', t_end='2030-01-02'),
        EventWindow('baz', t_start='2033-01-01', t_end='2033-01-02'),
    ]

    # Multi events
    funcs = [
        list,
        tuple,
        EventsList,
        EventsDict,
    ]

    for func in funcs:
        events = group(func(events_list))

        assert isinstance(events, EventsDict)
        assert len(events) == 3

        assert list(events.keys()) == ['bar', 'foo', 'baz']
        assert len(events['foo']) == 2

    # Invalid type
    with raises(TypeError):
        group('foo')


def test_events_concatenate():
    """Test events concatenate method."""
    event_dict = EventsDict(
        [
            EventWindow('foo', t_start='2032-01-01', t_end='2032-01-02'),
            EventWindow('bar', t_start='2031-01-01', t_end='2031-01-02'),
        ]
    )

    event_list = EventsList(
        [
            EventWindow('baz', t_start='2033-01-01', t_end='2033-01-02'),
            EventWindow('foo', t_start='2030-01-01', t_end='2033-01-01'),
        ]
    )

    # Group by event keys (and sort events)
    events = concatenate(event_dict, event_list, overlap=True)

    assert isinstance(events, EventsDict)
    assert len(events) == 3

    assert list(events.keys()) == ['foo', 'bar', 'baz']
    assert len(events['foo']) == 2
    assert [event.start for event in events['foo']] == [
        dt('2030-01-01'),
        dt('2032-01-01'),
    ]

    # Flatten events (and sort)
    events = concatenate(event_dict, event_list, flat=True, overlap=True)

    assert isinstance(events, EventsList)
    assert len(events) == 4

    assert [event.key for event in events] == ['foo', 'bar', 'foo', 'baz']
    assert [event.start for event in events] == [
        dt('2030-01-01'),
        dt('2031-01-01'),
        dt('2032-01-01'),
        dt('2033-01-01'),
    ]

    # Raise error when blocks overlap each other.
    err = (
        r'Overlap between `foo \(2030-01-01 -> 2033-01-01\)` '
        r'and `bar \(2031-01-01 -> 2031-01-02\)`'
    )
    with raises(ValueError, match=err):
        concatenate(event_dict, event_list)

    # don't raise error if no overlap (consecutive blocks)
    events = concatenate(event_list)

    assert [event.key for event in events] == ['foo', 'baz']

    # Concatenate zeros blocks
    events = concatenate()

    assert not events
