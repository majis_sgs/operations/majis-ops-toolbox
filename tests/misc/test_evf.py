"""Test miscellaneous event file module."""

from pathlib import Path

from numpy import datetime64 as dt

from majis.misc import read_evf
from majis.misc.evf import ref_count, ref_key

from pytest import raises

DATA = Path(__file__).parent / '..' / 'data'


def test_evf_ref_count():
    """Test EVF reference and count parser."""
    # Uppercase reference name and integer count
    ref, count = ref_count('Perijove_12PJ (COUNT = 1)')
    assert ref == 'PERIJOVE_12PJ'
    assert count == 1

    # Extra spaces
    ref, count = ref_count('CA_EUROPA  (COUNT  =  2)')
    assert ref == 'CA_EUROPA'
    assert count == 2

    # No spaces
    ref, count = ref_count('CA_EUROPA(COUNT=4)')
    assert ref == 'CA_EUROPA'
    assert count == 4

    # Invalid reference
    err = r'Invalid EVF key pattern: `FOO`\. Should be `REF_NAME \(COUNT = N\)`'
    with raises(KeyError, match=err):
        ref_count('FOO')


def test_evf_reader():
    """Test EVF file parser."""
    # With event file
    assert read_evf(DATA / 'events.evf') == {
        ('PERIJOVE_12PJ', 1): dt('2032-09-24T21:33:36'),
        ('CA_EUROPA', 1): dt('2031-01-22T18:54:03'),
        ('CA_EUROPA', 2): dt('2031-01-26T08:08:00'),
        ('CA_EUROPA', 3): dt('2031-01-29T21:21:56'),
    }

    # With a string
    assert read_evf('24-SEP-2032_21:33:36 PERIJOVE_12PJ (COUNT = 1)') == {
        ('PERIJOVE_12PJ', 1): dt('2032-09-24T21:33:36'),
    }

    # With a string (multi-lines)
    assert read_evf(
        '24-SEP-2032_21:33:36 PERIJOVE_12PJ (COUNT = 1)'
        '\n'
        '22-JAN-2031_18:54:03 CA_EUROPA (COUNT = 1)'
    ) == {
        ('PERIJOVE_12PJ', 1): dt('2032-09-24T21:33:36'),
        ('CA_EUROPA', 1): dt('2031-01-22T18:54:03'),
    }

    # With a list of strings
    assert read_evf(
        [
            '22-JAN-2031_18:54:03 CA_EUROPA (COUNT = 1)',
            '2031-01-26T08:08:00  CA_EUROPA (COUNT = 2)',
            '29-JAN-2031_21:21:56 CA_EUROPA (COUNT = 3)',
        ]
    ) == {
        ('CA_EUROPA', 1): dt('2031-01-22T18:54:03'),
        ('CA_EUROPA', 2): dt('2031-01-26T08:08:00'),
        ('CA_EUROPA', 3): dt('2031-01-29T21:21:56'),
    }

    # With a list of strings
    assert read_evf(None) is None


def test_evf_ref_key():
    """Test EVF reference key formatter."""
    # Input as str
    key, t_ref = ref_key('24-SEP-2032_21:33:36 PERIJOVE_12PJ  (COUNT  =  1)')

    assert key == 'PERIJOVE_12PJ (COUNT = 1)'
    assert t_ref == dt('2032-09-24T21:33:36')

    # Input as dict
    key, t_ref = ref_key({('CA_EUROPA', 2): dt('2031-01-26T08:08:00')})

    assert key == 'CA_EUROPA (COUNT = 2)'
    assert t_ref == dt('2031-01-26T08:08:00')

    # Input as tuple
    key, t_ref = ref_key(('CA_EUROPA (COUNT = 3)', dt('2031-01-29T21:21:56')))

    assert key == 'CA_EUROPA (COUNT = 3)'
    assert t_ref == dt('2031-01-29T21:21:56')

    # Too many references
    err = r'Only 1 reference could be provided, not 2'
    with raises(ValueError, match=err):
        ref_key(
            '24-SEP-2032_21:33:36 PERIJOVE_12PJ (COUNT = 1)'
            '\n'
            '22-JAN-2031_18:54:03 CA_EUROPA (COUNT = 1)'
        )

    # Invalid pattern
    with raises(ValueError):
        ref_key('FOO')
