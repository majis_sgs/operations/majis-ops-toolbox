"""Test miscellaneous time module."""

from pathlib import Path

from numpy import datetime64 as dt
from numpy import timedelta64 as td

from majis.misc import fmt_datetime, get_datetime
from majis.misc.time import fmt_timedelta

from pytest import raises

DATA = Path(__file__).parent / '..' / 'data'


def test_time_datetime_abs():
    """Test datetime getter with absolute time."""
    assert get_datetime('2032-09-23T05:15:45') == dt('2032-09-23T05:15:45')
    assert get_datetime('2032-09-23T05:15:46.000') == dt('2032-09-23T05:15:46')
    assert get_datetime('2032-09-23T05:15:47.000Z') == dt('2032-09-23T05:15:47')
    assert get_datetime('2032-09-23T05:15:48Z') == dt('2032-09-23T05:15:48')

    assert get_datetime('2032-09-23') == dt('2032-09-23')
    assert get_datetime('2032-09-23_05:15:49') == dt('2032-09-23T05:15:49')
    assert get_datetime('2032-SEP-23_05:15:50') == dt('2032-09-23T05:15:50')
    assert get_datetime('23-SEP-2032_05:15:51') == dt('2032-09-23T05:15:51')

    # Warning if a space is present only the date part is parsed
    assert get_datetime('2032-09-23 05:15:52') == dt('2032-09-23')

    # With dummy reference value(s) -> not used in absolute datetime:
    assert get_datetime('2032-09-23T05:15:45', refs={'foo': 'bar'}) == dt(
        '2032-09-23T05:15:45'
    )

    # Full string
    assert get_datetime(
        ' 2032-09-23T05:15:45.000Z  MAJIS   OBS_START  MAJ_JUP_DISK_SCAN_001 (PRIME=TRUE)'
    ) == dt('2032-09-23T05:15:45')

    # Invalid datetime input
    with raises(ValueError):
        _ = get_datetime('foo')


def test_time_datetime_rel():
    """Test datetime getter with relative time."""
    # String reference with full pattern
    assert get_datetime(
        'Perijove_12PJ  (COUNT = 1)  -001.18:35:25.000',
        refs='24-SEP-2032_21:33:36 PERIJOVE_12PJ (COUNT = 1)',
    ) == dt('2032-09-23T02:58:11')

    # List references and pattern without days nor milliseconds
    assert get_datetime(
        'PERIJOVE_12PJ  (COUNT = 1)  -18:35:20',
        refs=[
            '24-SEP-2032_21:33:36 PERIJOVE_12PJ (COUNT = 1)',
        ],
    ) == dt('2032-09-24T02:58:16')

    # Path name to .evf file and hours > 24
    assert get_datetime(
        'PERIJOVE_12PJ  (COUNT = 1)  -42:35:25.001',
        refs=DATA / 'events.evf',
    ) == dt('2032-09-23T02:58:10.999')

    # Dict references and change key spaces and + sign and full string
    assert get_datetime(
        'CA_EUROPA (COUNT  =  2)  +00:00:00.01 FOO BAR BAZ',
        refs={
            ('CA_EUROPA', 1): dt('2031-01-22T18:54:03'),
            ('CA_EUROPA', 2): dt('2031-01-26T08:08:00'),
            ('CA_EUROPA', 3): dt('2031-01-29T21:21:56'),
        },
    ) == dt('2031-01-26T08:08:00.010')

    # No reference for relative time
    err = r'Unknown time reference: `CA_EUROPA \(COUNT = 2\)`'
    with raises(KeyError, match=err):
        _ = get_datetime('CA_EUROPA (COUNT=2) 00:00:00')

    # Unknown reference time
    with raises(KeyError, match=err):
        _ = get_datetime(
            'CA_EUROPA (COUNT=2) 00:00:00',
            refs='22-JAN-2031_18:54:03 CA_EUROPA (COUNT = 1)',
        )


def test_time_fmt_timedelta():
    """Test time delta formatter."""
    # Default +DDD.hh:mm:ss.ms
    assert fmt_timedelta(td(1, 'D')) == '+001.00:00:00.000'
    assert fmt_timedelta(td(1, 'h')) == '+000.01:00:00.000'
    assert fmt_timedelta(td(1, 'm')) == '+000.00:01:00.000'
    assert fmt_timedelta(td(1, 's')) == '+000.00:00:01.000'
    assert fmt_timedelta(td(1, 'ms')) == '+000.00:00:00.001'

    # Zero values
    assert fmt_timedelta(td(0, 'ms')) == '+000.00:00:00.000'
    assert fmt_timedelta(-td(0, 'ms')) == '+000.00:00:00.000'

    # Negative values
    assert fmt_timedelta(-td(1, 'ms')) == '-000.00:00:00.001'
    assert fmt_timedelta(-td(1, 'D')) == '-001.00:00:00.000'

    assert fmt_timedelta(td(25, 'h')) == '+001.01:00:00.000'
    assert fmt_timedelta(td(61, 'm')) == '+000.01:01:00.000'
    assert fmt_timedelta(td(61, 's')) == '+000.00:01:01.000'
    assert fmt_timedelta(td(1001, 'ms')) == '+000.00:00:01.001'

    assert (
        fmt_timedelta(td(1234, 'D') + td(5, 'h') + td(6, 'm') + td(7, 's') + td(8, 'ms'))
        == '+1234.05:06:07.008'
    )


def test_time_fmt_datetime():
    """Test time delta formatter."""
    assert fmt_datetime('2032-09-23T05:15:45') == '2032-09-23T05:15:45.000Z'
    assert fmt_datetime(dt('2032-09-23T05:15:46')) == '2032-09-23T05:15:46.000Z'

    assert (
        fmt_datetime(
            '2032-09-23T02:58:11', ref='24-SEP-2032_21:33:36 Perijove_12PJ (COUNT = 1)'
        )
        == 'PERIJOVE_12PJ (COUNT = 1)  -001.18:35:25.000'
    )

    assert (
        fmt_datetime(
            '2031-01-21T17:53:01.999', ref='22-JAN-2031_18:54:03 CA_EUROPA (COUNT = 1)'
        )
        == 'CA_EUROPA (COUNT = 1)  -001.01:01:01.001'
    )

    assert fmt_datetime(
        '2031-01-26T08:08:01',
        '2031-01-25T08:07:59.999',
        ref='25-JAN-2031_08:08:00 CA_EUROPA (COUNT = 2)',
    ) == [
        'CA_EUROPA (COUNT = 2)  +001.00:00:01.000',
        'CA_EUROPA (COUNT = 2)  -000.00:00:00.001',
    ]
